package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations{ 

	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}
	@FindAll({@FindBy(how=How.ID, using="username"),
	@FindBy(how=How.CLASS_NAME, using="inputLogin")}) WebElement userName;
	
	@CacheLookup
	@FindBy(how=How.ID, using="username")  WebElement eleUserName;
	@FindBy(how=How.ID, using="password")  WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;

	public LoginPage enterUserName(String data) {
		//		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, data);  
		return this; 
	}
	public LoginPage enterPassWord(String data) {
		//		WebElement elePassWord = locateElement("id", "password");
		clearAndType(elePassWord, data); 
		return this; 
	}
	public HomePage clickLogin() {
		//		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);  
		return new HomePage();
	}
}


/*@When("Click on the login button")
@And("Enter the password as (.*)")
@Given("Enter the Username as (.*)")*/




